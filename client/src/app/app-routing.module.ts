import { AddClubComponent } from './addClub/addClub.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegisterComponent } from './register/register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { ClubTableComponent } from './club-table/club-table.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'naslovna', component: HomePageComponent },
  { path: 'stolovi', component: ClubTableComponent },
  { path: 'stolovi', component: ClubTableComponent },
  { path: 'dodajKlub', component: AddClubComponent},
  { path: 'profil', component: ProfileComponent },
  { path: '', redirectTo: '/naslovna', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
