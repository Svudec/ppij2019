import { Club } from 'src/app/models/Club';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ClubService } from '../services/ClubService/Club.service';
import { Router } from '@angular/router';
import { User } from '../models/User';

@Component({
  selector: 'app-addClub',
  templateUrl: './addClub.component.html',
  styleUrls: ['./addClub.component.css']
})
export class AddClubComponent implements OnInit {
  private clubForm = new FormGroup({
    name: new FormControl(''),
    location: new FormControl(''),
    description: new FormControl(''),
    oib: new FormControl(''),
    ownerOib: new FormControl('')
  });

  errorMessage: string;


  constructor(private clubService: ClubService, private router: Router) { }

  ngOnInit() {
  }

  registerClub() {
    this.clubService
      .registerClub(
        new Club(
          this.clubForm.get('name').value,
          this.clubForm.get('location').value,
          this.clubForm.get('description').value,
          this.clubForm.get('oib').value,
          this.clubForm.get('ownerOib').value
        )
      ).subscribe(() => {
        const usera = JSON.parse(localStorage.getItem('currentUser'));
        usera.role = 'CLUB_OWNER';
        localStorage.removeItem('currentUser');
        console.log(usera);
        this.clubService.myClub().subscribe(msg => {
          this.clubService.clubId = msg.body.clubId;
          console.log(this.clubService.clubId);
        });
        localStorage.setItem('currentUser', JSON.stringify(usera));
        this.router.navigateByUrl('/naslovna');
      },
        error => {
          this.errorMessage = error.error.error;
        }
      );
  }

}
