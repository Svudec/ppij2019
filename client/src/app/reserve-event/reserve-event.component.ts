import { Subscription } from 'rxjs';
import { Reservation } from './../models/Reservation';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ClubTableService } from '../services/ClubTableService/club-table.service';
import { EventItemService } from '../services/EventItem/event-item.service';
import { ClubTable } from '../models/ClubTable';
import { MatSelectionList } from '@angular/material/list';
import { ReserveService } from '../services/reserve-service/reserve.service';

@Component({
  selector: 'app-reserve-event',
  templateUrl: './reserve-event.component.html',
  styleUrls: ['./reserve-event.component.css']
})

export class ReserveEventComponent implements OnInit {
  @Input() currentEvent: any;
  @ViewChild( MatSelectionList, null ) selectionList: MatSelectionList;
  tableList: Array<ClubTable>;
  selectedList = Array<ClubTable>();
  errorMessage: string;
  getAllClubTablesSubscription: Subscription;

  constructor(private tableService: ClubTableService,
              private eventItemService: EventItemService,
              private reserveService: ReserveService) {
    this.eventItemService.getCurrentEventObservable()
    .subscribe(event => {
      this.currentEvent = event;
      this.getTables();
    });
    this.getAllClubTablesSubscription = this.reserveService.getAllClubTable().subscribe(tables => {
      console.log(tables);
      this.tableList = tables;
    });
  }

  ngOnInit() {
    console.log(this.currentEvent);
    this.getTables();
    console.log(this.selectedList);
  }


  getTables() {
    this.tableService.collectAllClubTablesReservation(this.currentEvent.eventId)
    .subscribe(tables => {
      console.log(this.currentEvent.clubDTO.clubId);
      this.tableList = tables;
    });
  }

  onSelectTable(table: ClubTable) {
    console.log(table);
  }

  confirmSelected() {
    console.log(this.selectedList);
    for (let i = this.tableList.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedList.length; j++) {
      if (this.tableList[i] === this.selectedList[j]) {
          this.tableList.splice(i, 1);
          // break;       //<-- Uncomment  if only the first term has to be removed
      }
    }
  }
    this.selectedList
    .map(table => {
      this.reserveService
      .setReservation(
        new Reservation(
          null,
          table.clubTableId,
          this.currentEvent.eventId
        )
      ).subscribe(response => console.log(response),
      error => {
        if (error.status === 400) {
          this.errorMessage = 'Pogreška pri slanju zahtjeva';
        } else {
          this.errorMessage = error.error.error;
        }
      });
    });

  }
}
