import { ClubService } from './../services/ClubService/Club.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/UserService/User.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  errorMessage: string;
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  clubId: number;

  constructor(private userService: UserService, private router: Router, private clubService: ClubService) { }

  ngOnInit() {
  }

  login() {
    this.userService.loginUser(
      this.loginForm.get('email').value,
      this.loginForm.get('password').value
    ).subscribe(response => {
      localStorage.setItem('currentUser', JSON.stringify(response.body.user_data));
      localStorage.setItem('token', response.body.access_token);
      console.log(response.body.user_data);
      if (response.body.user_data.role === 'CLUB_OWNER') {
      this.clubService.myClub().subscribe( msg => {
        this.clubService.clubId = msg.body.clubId;
        console.log(this.clubService.clubId);
      });
    }
    },
    error => {
      if (error.status) {
        this.errorMessage = 'Nemate pristup';
      }
    });
  }
}
