import { ClubService } from './../services/ClubService/Club.service';
import { ClubTable } from './../models/ClubTable';
import { Component, OnInit, Input } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../services/UserService/User.service';
import { Router } from '@angular/router';
import { ClubTableService } from '../services/ClubTableService/club-table.service';
import { ClubTableItemService } from '../services/ClubTableItemService/club-table-item.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-club-table',
  templateUrl: './club-table.component.html',
  styleUrls: ['./club-table.component.css']
})
export class ClubTableComponent implements OnInit {
  user: User;
  @Input() tableList: Array<ClubTable>;
  currentTable: ClubTable;
  createClubTableSubscription: Subscription;
  deleteClubTableSubscription: Subscription;

  constructor(private router: Router, private userService: UserService, private clubTableService: ClubTableService,
              private clubTableItemService: ClubTableItemService, private clubService: ClubService) {

    clubTableService.collectAllClubTables(this.clubService.clubId.toString()).subscribe(data => {
      console.log(data.slice());
      this.tableList = new Array<ClubTable>();
      this.tableList = data.slice();
      this.clubTableService.clubTables = this.tableList;
      console.log(this.tableList);
    });
    this.createClubTableSubscription = this.clubTableService.getClubTablesObservable().subscribe(msg => {
      clubTableService.collectAllClubTables(this.clubService.clubId.toString()).subscribe(data => {
        console.log(data.slice());
        this.tableList = new Array<ClubTable>();
        this.tableList = data.slice();
        this.clubTableService.clubTables = this.tableList;
        console.log(this.tableList);
      });
    });
    this.deleteClubTableSubscription = this.clubTableItemService.getDeleteClubTableObservable().subscribe(clubTableItem => {
      for (let i = 0; i < this.tableList.length; i++) {
        if (this.tableList[i] === clubTableItem) {
          this.tableList.splice(i, 1);
        }
      }
    });

  }

  ngOnInit() {
    this.clubTableService.ClubTablesSubject.next();
  }

  onSelectClubTable(clubTableId: number) {
    console.log(clubTableId);
    this.currentTable = this.clubTableItemService.getCurrentClubTable(clubTableId);
    this.clubTableItemService.isItCreateSubject.next(false);
    console.log(this.currentTable);
  }

  addNewClubTableButton() {
    let newTable: ClubTable;
    newTable = {
      clubTableId: null,
      size: null,
      description: null
    };
    this.currentTable = newTable;
    console.log(this.currentTable);
    this.clubTableItemService.currentClubTableSubject.next(this.currentTable);
    this.clubTableItemService.isItCreateSubject.next(true);
    this.currentTable = newTable;

  }

}
