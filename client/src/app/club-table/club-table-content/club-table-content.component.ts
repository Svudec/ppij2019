import { ClubService } from './../../services/ClubService/Club.service';
import { ClubTableService } from './../../services/ClubTableService/club-table.service';
import { ClubTable } from '../../models/ClubTable';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ClubTableItemService } from 'src/app/services/ClubTableItemService/club-table-item.service';

@Component({
  selector: 'app-club-table-content',
  templateUrl: './club-table-content.component.html',
  styleUrls: ['./club-table-content.component.css']
})
export class ClubTableContentComponent implements OnInit {
  @ViewChild('renderedClubTable', {static: true}) currentClubTable: ClubTable;
  showForm = false; // poslje stavi false i promjeni pomocu metode
  isItCreate: boolean;
  currentClubTableSubscription: Subscription;
  isItCreateSubscription: Subscription;
  newClubTable: ClubTable;
  errorMessage: string;

  createForm = new FormGroup({
    size: new FormControl(''),
    description: new FormControl('')
  });

  constructor(private router: Router, private clubTableService: ClubTableService, private clubTableItemService: ClubTableItemService,
              private clubService: ClubService) {
    this.currentClubTable = {
      clubTableId: null,
      size: null,
      description: null,
    };
    this.currentClubTableSubscription = this.clubTableItemService.getCurrentClubTableObservable().subscribe(currentClubTable => {
      this.currentClubTable = currentClubTable;
      this.showForm = true;
      console.log(this.currentClubTable);
    });
    this.isItCreateSubscription = this.clubTableItemService.getIsItCreateObservable().subscribe(isItCreate => {
      this.isItCreate = isItCreate;
      console.log(isItCreate);
    });
  }

  ngOnInit() {
  }

  onCreateClubTable() {
    this.clubTableItemService.isItCreateSubject.next(false);
    this.clubTableService.createClubTable(this.currentClubTable).subscribe(data => {
      console.log('l6: ', data);
      /*if (data.error) {
        return;
        }
      */
      this.clubTableService.ClubTablesSubject.next();
      // this.resetItem();
      });
  }

  onDeleteClubTable() {
    console.log(this.currentClubTable);
    this.clubTableItemService.deleteClubTable(this.currentClubTable).subscribe(data => {
      console.log(data);
      this.clubTableService.collectAllClubTables(this.clubService.clubId.toString());
    }, error => {
      this.errorMessage = error.error.error;
    }
    );
  }


  onUpdateClubTable() {
    this.clubTableItemService.updateItem(this.currentClubTable).subscribe(data => {
      console.log('l8: ', data, 'sub', this.clubTableService.ClubTablesSubject);
      this.clubTableService.ClubTablesSubject.next();
    });


  }

}
