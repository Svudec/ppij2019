import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubTableContentComponent } from './club-table-content.component';

describe('ClubTableItemComponent', () => {
  let component: ClubTableContentComponent;
  let fixture: ComponentFixture<ClubTableContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubTableContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubTableContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
