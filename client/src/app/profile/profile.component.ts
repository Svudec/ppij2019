import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from './../services/UserService/User.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/User';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;
  editEmail = false;
  editUsername = false;
  editHappened = false;
  edit = new FormGroup({
    username: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private userService: UserService,
              private router: Router) {
    this.userService.getMe()
    .subscribe((response) => {
      console.log(response.body);
      this.user = response.body;
    });
  }

  ngOnInit() {
  }

  back() {
    this.router.navigateByUrl('/naslovna');
  }

  toggleEditEmail() {
    this.editHappened = true;
    this.editEmail = !this.editEmail;
  }

  toggleEditUsername() {
    this.editHappened = true;
    this.editUsername = !this.editUsername;
  }

  saveEmail() {
    this.user.email = this.edit.get('email').value;
    this.editEmail = false;
    console.log(this.user.email);
  }

  saveUsername() {
    this.user.username = this.edit.get('username').value;
    this.editUsername = false;
    console.log(this.user.username);
  }

  saveChanges() {
    this.userService.editUser(this.user);
    this.router.navigateByUrl('/naslovna');
  }
}
