export class Reservation {
    reservationId: number;
    tableId: number;
    eventId: number;

    constructor(reservationId: number, tableId: number, eventId: number) {
        this.reservationId = reservationId;
        this.tableId = tableId;
        this.eventId = eventId;
    }
}
