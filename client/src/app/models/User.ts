export class User {
  username: string;
  password: string;
  email: string;
  name: string;
  surname: string;
  dateOfBirth: string;

  constructor(username: string, firstName: string, lastName: string, email: string, password: string, dateOfBirth: string) {
    this.username = username;
    this.name = firstName;
    this.surname = lastName;
    this.email = email;
    this.password = password;
    this.dateOfBirth = dateOfBirth;
  }
}
