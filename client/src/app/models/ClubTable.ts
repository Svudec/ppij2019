export class ClubTable {
    clubTableId: number;
    size: number;
    description: string;


constructor(id: number, size: number, description: string) {
    this.clubTableId = id;
    this.size = size;
    this.description = description;
  }
}
