export class Club {
  name: string;
  location: string;
  description: string;
  oib: string;
  ownerOib: string;

  constructor(name: string, location: string, description: string, oib: string, ownerOib: string) {
    this.name = name;
    this.location = location;
    this.description = description;
    this.oib = oib;
    this.ownerOib = ownerOib;
  }
}
