export class EventItem {
    eventId: number;
    name: string;
    date: string;
    description: string;


constructor(id: number, name: string, date: string, description: string) {
    this.eventId = id;
    this.name = name;
    this.date = date;
    this.description = description;
  }
}
