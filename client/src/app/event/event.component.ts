import { ClubService } from './../services/ClubService/Club.service';
import { UserService } from './../services/UserService/User.service';
import { Component, OnInit, Input } from '@angular/core';
import { User } from '../models/User';
import { Router } from '@angular/router';
import { EventItemService } from '../services/EventItem/event-item.service';
import { EventItem } from '../models/EventItem';
import { EventService } from '../services/EventService/Event.service';
import { Subject, Subscription } from 'rxjs';
import { Club } from '../models/Club';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  clubLocations: string[];
  currentLocation: string;
  user: User;
  @Input() eventList: Array<EventItem>;
  currentClubs: Array<Club>;
  currentEvents: Array<EventItem>;
  currentEvent: EventItem;
  createEventSubscription: Subscription;
  deleteEventSubscription: Subscription;
  currentClubsSubscription: Subscription;
  currentEventsSubscription: Subscription;

  constructor(private router: Router, private userService: UserService, private eventService: EventService,
              private eventItemService: EventItemService, private clubService: ClubService) {
    this.createEventSubscription = this.eventService.getEventsObservable().subscribe(msg => {
      if (JSON.parse(localStorage.getItem('currentUser')).role === 'USER') {
        eventService.collectAllEvents().subscribe(data => {
          this.eventList = data;
        });
      } else {
        eventService.collectAllMyEvents().subscribe(data => {
          this.eventList = data;
        });
      }
    });
    this.deleteEventSubscription = this.eventItemService.getDeleteEventObservable().subscribe(eventItem => {
      for (let i = 0; i < this.eventList.length; i++) {
        if (this.eventList[i] === eventItem) {
          this.eventList.splice(i, 1);
        }
      }
    });
    this.currentClubsSubscription = this.clubService.getCurrentClubs().subscribe(clubs => {
      this.currentClubs = clubs;
      console.log(this.currentClubs);
    });

    this.currentEventsSubscription = this.eventService.getCurrentEventsObservable().subscribe(events => {
      console.log(events);
      this.currentEvents = events;
    });
  }

  ngOnInit() {
    this.eventService.EventsSubject.next();
    if (!this.userService.isClubOwner()) {
      this.clubService.getLocations()
        .subscribe(locations => {
          this.clubLocations = locations;
          console.log(this.clubLocations);
        });
    }
  }

  onSelectClub(clubId: string ) {
    console.log(clubId);
    this.eventService.collectAllCurrentEvents(clubId).subscribe(events => {
      this.currentEvents = events;
      console.log(events);
    });
    //this.eventService.collectAllCurrentEvents(clubId).subscribe(events => {
    //  this.eventService.currentEventsSubject.next(events);
      //this.currentEvents = events;
    //});
  }

  onClick(location: string) {
    console.log(location);
    this.currentLocation = location;
    this.clubService.setClubsLocation(location);
    this.currentClubs = this.clubService.currentClubs;
  }

  onSelectEvent(event: EventItem) {
    this.currentEvent = event;
    this.eventItemService.setCurrentEvent(event);
    //this.eventItemService.reservationSubject.next(event.id);
    console.log(event);
    this.eventItemService.isItCreateSubject.next(false);
    
  }

  addNewEventButton() {
    const newEvent = {
      eventId: null,
      name: 'Novi događaj',
      description: null,
      date: null,
    };
    this.currentEvent = newEvent;
    console.log(this.currentEvent);
    this.eventItemService.currentEventSubject.next(this.currentEvent);
    this.eventItemService.isItCreateSubject.next(true);
    this.currentEvent = newEvent;
  }

  

}
