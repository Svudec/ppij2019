import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { EventItem } from 'src/app/models/EventItem';
import { Subscription, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { EventService } from 'src/app/services/EventService/Event.service';
import { EventItemService } from 'src/app/services/EventItem/event-item.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Reservation } from 'src/app/models/Reservation';
import { TouchSequence } from 'selenium-webdriver';
import { UserService } from 'src/app/services/UserService/User.service';


@Component({
  selector: 'app-event-content',
  templateUrl: './event-content.component.html',
  styleUrls: ['./event-content.component.css']
})
export class EventContentComponent implements OnInit {

  @ViewChild('renderedEvent', { static: true }) currentEvent: EventItem;
  showForm = false; // poslje stavi false i promjeni pomocu metode
  @Input() reservationList: Array<Reservation>;
  isItCreate: boolean;
  newEvent: EventItem;
  reserveClicked = false;
  reservationSubscription: Subscription;


  createForm = new FormGroup({
    name: new FormControl(''),
    date: new FormControl(''),
    description: new FormControl('')
  });

  // @ViewChild('selectionElementRef') selectionElementRef;
  // @ViewChild('comment') comment: string;


  constructor(private router: Router,
              private userService: UserService,
              private eventService: EventService,
              private eventItemService: EventItemService) {

    this.eventItemService
    .getCurrentEventObservable()
    .subscribe(currentEvent => {
      this.currentEvent = currentEvent;
      this.showForm = true;
      console.log(this.currentEvent);
      this.reservationSubscription = this.eventItemService.collectAllReservations(currentEvent.eventId.toString()).subscribe(data=>{
        this.reservationList = data;
      });
    });

    this.eventItemService.getReservationsObservable().subscribe(data => {
      this.eventItemService.collectAllReservations(data).subscribe(list => {
        console.log(list);
        this.reservationList = list;
      });
    });

    this.eventItemService.getIsItCreateObservable().subscribe(isItCreate => {
      this.isItCreate = isItCreate;
      console.log(isItCreate);
    });
    
  }

  ngOnInit() {  }

  resetItem() {
    this.currentEvent = {
      eventId: null,
      name: null,
      description: null,
      date: null,
    };

    // this.timeOption = null;
  }

  onCreateEvent() {
    this.eventItemService.isItCreateSubject.next(false);
    this.eventService.createEvent(this.currentEvent).subscribe(data => {
      console.log('l6: ', data);
      /*if (data.error) {
        return;
        }
      */
      this.eventService.EventsSubject.next();
      // this.resetItem();
    });
  }

  onDeleteEvent() {
    console.log(this.currentEvent);
    this.eventItemService.deleteEvent(this.currentEvent).subscribe(data => {
      console.log(data);
      this.eventService.collectAllEvents();
    });
  }

  toggleReserve() {
    this.reserveClicked = !this.reserveClicked;
  }


  onUpdateEvent() {
    this.eventItemService.updateItem(this.currentEvent).subscribe(data => {
      console.log('l8: ', data, 'sub', this.eventService.EventsSubject);
      this.eventService.EventsSubject.next();
    });


  }

  getReservations(id: string) {
    this.eventItemService.collectAllReservations(id).subscribe(data => this.reservationList = data);
  }

  reserveEvent() {
    console.log(this.currentEvent);
    this.router.navigateByUrl('/rezervacija');
  }

}
