import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../services/UserService/User.service';
import { User } from '../models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  private errorMessage: string;

  userForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    username: new FormControl(''),
    password: new FormControl(''),
    dateOfBirth: new FormControl('')
  });


  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.userService.registerUser(
      new User(
        this.userForm.get('username').value,
        this.userForm.get('firstName').value,
        this.userForm.get('lastName').value,
        this.userForm.get('email').value,
        this.userForm.get('password').value,
        this.userForm.get('dateOfBirth').value
      )
    ).subscribe(() => this.router.navigateByUrl('/naslovna') ,
    error => {
      this.errorMessage = error.error.error;
    });
  }
}
