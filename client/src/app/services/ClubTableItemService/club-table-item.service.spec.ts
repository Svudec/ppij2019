import { TestBed } from '@angular/core/testing';

import { ClubTableItemService } from './club-table-item.service';

describe('ClubTableItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClubTableItemService = TestBed.get(ClubTableItemService);
    expect(service).toBeTruthy();
  });
});
