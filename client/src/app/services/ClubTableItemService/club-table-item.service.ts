import { ClubTable } from './../../models/ClubTable';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { UserService } from '../UserService/User.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { ClubTableService } from '../ClubTableService/club-table.service';

@Injectable({
  providedIn: 'root'
})
export class ClubTableItemService {
  currentClubTableSubject = new Subject<ClubTable>();
  currentClubTable: ClubTable;
  isItCreateSubject = new Subject<boolean>();
  url = 'http://localhost:8080';
  deleteClubTableSubject = new Subject<any>();
  createClubTableSubject = new Subject<any>();
  updateClubTableSubject = new Subject<any>();

  constructor(private http: HttpClient, private userService: UserService, private clubTableService: ClubTableService) { }

  getCurrentClubTableObservable(): Observable<ClubTable> {
    return this.currentClubTableSubject.asObservable();
  }

  getCreateClubTableObservable(): Observable<ClubTable> {
    return this.createClubTableSubject.asObservable();
  }

  getIsItCreateObservable(): Observable<boolean> {
    return this.isItCreateSubject.asObservable();
  }

  getCurrentClubTable(id: number): ClubTable {
    console.log(id);
    this.currentClubTable = this.clubTableService.clubTables.find(x => x.clubTableId === id);
    console.log(this.currentClubTable);
    this.currentClubTableSubject.next(this.currentClubTable);
    //this.isItUpdateSubject.next(isItUpdate);
    return this.currentClubTable;
  }


  getDeleteClubTableObservable(): Observable<ClubTable> {
    return this.deleteClubTableSubject.asObservable();
  }

  deleteClubTable(item: ClubTable): Observable<any> {
    console.log(item);
    const params = new HttpParams()
                       .append('Id', item.clubTableId.toString());
    this.deleteClubTableSubject.next(item);
    const Id = item.clubTableId;
    return this.http.post<any>(this.url + '/table/delete', {},
    { observe: 'response',
    params: {
      tableId: item.clubTableId.toString()
    }, headers: {
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    'Content-Type': 'application/json',
  }
});
  }


  getUpdateObservable(): Observable<boolean> {
    return this.updateClubTableSubject.asObservable();
  }

  updateItem(item: ClubTable): Observable<any> {
    return this.http.post<any>(this.url + '/table/edit', item, {
      observe: 'response',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }
    });
  }

}
