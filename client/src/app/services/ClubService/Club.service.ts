import { EventComponent } from './../../event/event.component';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Club } from 'src/app/models/Club';
import { Observable, Subject } from 'rxjs';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class ClubService {
  private club = 'http://localhost:8080/myClub';
  private clubUrl = 'http://localhost:8080/club/new';
  private locationsUrl = 'http://localhost:8080/clubLocations';
  private clubsLocationUrl = 'http://localhost:8080/findClubs';
  currentClubs: Array<Club>;
  clubId: number;
  public clubSubject = new Subject<any>();
  currentClubsSubject = new Subject<Array<Club>>();


  constructor(private http: HttpClient) { }

  getCurrentClubs(): Observable<Array<Club>> {
    return this.currentClubsSubject.asObservable();
  }


  registerClub(club: Club): Observable<HttpResponse<Club>> {
    console.log(JSON.parse(localStorage.getItem('currentUser')));
    console.log({
      observe: 'response',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }
    });
    return this.http.post<Club>(this.clubUrl, club,
      {
        observe: 'response',
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
          'Content-Type': 'application/json',
        }
      });
  }


  myClub(): Observable<any> {
    return this.http.get<any>(this.club,
      {
        observe: 'response',
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
          'Content-Type': 'application/json',
        }
      });
  }

  setClubsLocation(clubLocation: string) {
    this.http.get<Club[]>(this.clubsLocationUrl, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }, params: {
        location: clubLocation
      }
    })
      .subscribe(clubs => {
        console.log(clubs);
        this.currentClubs = clubs;
        this.currentClubsSubject.next(clubs);
      });
  }

  getLocations(): Observable<string[]> {

    return this.http.get<string[]>(this.locationsUrl, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    });

  }

}
