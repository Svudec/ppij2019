import { ClubService } from './../ClubService/Club.service';
import { ClubTable } from './../../models/ClubTable';
import { Injectable, Input } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { UserService } from '../UserService/User.service';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class ClubTableService {
  activeClubTable = new Subject<ClubTable>();
  addClubTableSubject = new Subject<ClubTable>();
  activeClubTableName: string;
  activeClubTableid: number;
  activeClubTableItem: ClubTable;
  @Input() clubTables: Array<ClubTable> = new Array();
  public ClubTablesSubject = new Subject();

  constructor(private router: Router, private http: HttpClient, private userService: UserService, private clubService: ClubService) { }


getActiveClubTableObservable(): Observable<ClubTable> {
  return this.activeClubTable.asObservable();
}

getClubTablesObservable(): Observable<any> {
  return this.ClubTablesSubject.asObservable();
}

getAddClubTable(): Observable<ClubTable> {
  return this.addClubTableSubject.asObservable();
}

onSelectClubTable(clubTableId: number) {
  const clubTable = this.getActiveClubTable(clubTableId.toString());
  console.log(clubTable);
  this.activeClubTableItem = clubTable;
  this.activeClubTableid = clubTable.clubTableId;
  console.log(clubTable);
  this.activeClubTable.next(clubTable);
}

collectAllClubTables(clubId: string): Observable<Array<ClubTable>> {
  console.log(clubId);
  const param = new HttpParams().set('clubId', this.clubService.clubId.toString());
  return this.http.get<Array<ClubTable>>('http://localhost:8080/club/tables', {
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem('token'),
      'Content-Type': 'application/json',
    }, params: param
  }
  );
}

collectAllClubTablesReservation(clubId: string): Observable<Array<ClubTable>> {
  console.log(clubId);
  const param = new HttpParams().set('eventId', clubId.toString());
  return this.http.get<Array<ClubTable>>('http://localhost:8080/event/freeTables', {
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem('token'),
      'Content-Type': 'application/json',
    }, params: param
  }
  );
}

getActiveClubTable(clubTableId: string): ClubTable {
  return this.clubTables.find(x => x.clubTableId === parseInt(clubTableId, 10));
}

createClubTable(clubTable: ClubTable): Observable<any> {
  return this.http.post<any>('http://localhost:8080/table/new', clubTable,
    {
      observe: 'response',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }
    });
}

}
