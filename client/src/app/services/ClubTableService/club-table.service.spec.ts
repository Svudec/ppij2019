import { TestBed } from '@angular/core/testing';

import { ClubTableService } from './club-table.service';

describe('ClubTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClubTableService = TestBed.get(ClubTableService);
    expect(service).toBeTruthy();
  });
});
