import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';


import { User } from 'src/app/models/User';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private registerUrl = 'http://localhost:8080/register';
  private authUrl = 'http://localhost:8080/oauth/token';
  private meUrl = 'http://localhost:8080/me';
  private editUserUrl = 'http://localhost:8080/user/edit';
  clubId: number;

constructor(private http: HttpClient) { }

  registerUser(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>(this.registerUrl, user, { observe: 'response' });
  }

  loginUser(username: string, password: string): Observable<HttpResponse<any>> {
    const userParams = new HttpParams()
    .append('grant_type', 'password')
    .append('username', username)
    .append('password', password);

    const header = {
      Authorization: 'Basic ' + btoa('user:user'),
      'Content-Type': 'application/x-www-form-urlencoded'
    };

    return this.http.post<any>(this.authUrl, userParams, { headers: header, observe: 'response' });

  }

  getMe(): Observable<HttpResponse<User>> {
    return this.http.get<User>(this.meUrl, {
      observe: 'response',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-type': 'application/json'
      }
    });
  }

  editUser(user: User) {
    return this.http.put<User>(this.editUserUrl, user, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-type': 'application/json'
      }
    }).subscribe(() => console.log('Success'));
  }


  isUserLoggedIn() {
    return localStorage.getItem('currentUser');
  }

  getLoggedUser(): User {
    return JSON.parse( localStorage.getItem('currentUser') ) as User;
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
  }

  isClubOwner() {
    return JSON.parse(localStorage.getItem('currentUser')).role === 'CLUB_OWNER';
  }

}
