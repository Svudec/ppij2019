import { EventItem } from 'src/app/models/EventItem';
import { Injectable, Input } from '@angular/core';
import { EventContentComponent } from 'src/app/event/event-content/event-content.component';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserService } from '../UserService/User.service';
import { User } from 'src/app/models/User';
import { Club } from 'src/app/models/Club';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  activeEvent = new Subject<EventItem>();
  addEventSubject = new Subject<EventItem>();
  currentEvents: Array<EventItem>;


  activeEventName: string;
  activeEventid: string;
  activeEventItem: EventItem;
  public EventsSubject = new Subject<Array<EventItem>>();
  currentEventsSubject = new Subject<Array<EventItem>>();

  constructor(private router: Router, private http: HttpClient, private userService: UserService) { }

  getCurrentEventsObservable(): Observable<Array<EventItem>> {
    return this.currentEventsSubject.asObservable();
  }

  getActiveEventObservable(): Observable<EventItem> {
    return this.activeEvent.asObservable();
  }

  getEventsObservable(): Observable<Array<EventItem>> {
    return this.EventsSubject.asObservable();
  }

  getAddEvent(): Observable<EventItem> {
    return this.addEventSubject.asObservable();
  }

  collectAllEvents(): Observable<Array<EventItem>> {
    return this.http.get<Array<EventItem>>('http://localhost:8080/events', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }
    }
    );
  }

  collectAllCurrentEvents(clubid: string): Observable<Array<EventItem>> {
    return this.http.get<Array<EventItem>>('http://localhost:8080/currentEvents', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }, params: {
        clubId: clubid
      }
    }
    );
  }

  collectAllMyEvents(): Observable<Array<EventItem>> {
    return this.http.get<Array<EventItem>>('http://localhost:8080/myEvents', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }
    }
    );
  }

  getActiveEvent(): EventItem {
    return this.activeEventItem;
  }

  createEvent(event: EventItem): Observable<HttpResponse<Event>> {
    return this.http.post<Event>('http://localhost:8080/event/new', event,
      {
        observe: 'response',
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
          'Content-Type': 'application/json',
        }
      });
  }

}
