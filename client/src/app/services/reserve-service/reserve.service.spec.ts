/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ReserveService } from './reserve.service';

describe('Service: Reserve', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReserveService]
    });
  });

  it('should ...', inject([ReserveService], (service: ReserveService) => {
    expect(service).toBeTruthy();
  }));
});
