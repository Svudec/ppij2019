import { Reservation } from './../../models/Reservation';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { ClubTable } from 'src/app/models/ClubTable';

@Injectable({
  providedIn: 'root'
})
export class ReserveService {

  private newReservationUrl = 'http://localhost:8080/reservation/new';
  getAllClubTables = new Subject<Array<ClubTable>>();

  constructor(private http: HttpClient) { }

  setReservation(reservation: Reservation) {

    return this.http.post<Reservation>(this.newReservationUrl, reservation,
      { headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      },
    observe: 'response'
  });

  }

  getAllClubTable(): Observable<Array<ClubTable>> {
    return this.getAllClubTables.asObservable();
  }

}
