import { UserService } from './../UserService/User.service';
import { Injectable } from '@angular/core';
import { EventItem } from 'src/app/models/EventItem';
import { Observable, Subject } from 'rxjs';
import { User } from 'src/app/models/User';
import { HttpClient, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { EventService } from '../EventService/Event.service';
import { Reservation } from 'src/app/models/Reservation';

@Injectable({
  providedIn: 'root'
})
export class EventItemService {
  currentEventSubject = new Subject<EventItem>();
  currentEvent: EventItem;
  isItCreateSubject = new Subject<boolean>();
  url = 'http://localhost:8080';
  deleteEventSubject = new Subject<any>();
  createEventSubject = new Subject<any>();
  updateEventSubject = new Subject<any>();
  reservationSubject = new Subject<any>();

  constructor(private http: HttpClient, private userService: UserService, private eventService: EventService) { }

  getCurrentEventObservable(): Observable<EventItem> {
    return this.currentEventSubject.asObservable();
  }

  getCreateEventObservable(): Observable<EventItem> {
    return this.createEventSubject.asObservable();
  }

  getIsItCreateObservable(): Observable<boolean> {
    return this.isItCreateSubject.asObservable();
  }

  getReservationsObservable(): Observable<any> {
    return this.reservationSubject.asObservable();
  }

  setCurrentEvent(event: EventItem): EventItem {
    console.log(event);
    this.currentEventSubject.next(event);
    //this.reservationSubject.next(event.id.toString());
    // this.isItUpdateSubject.next(isItUpdate);
    return this.currentEvent;
  }


  getDeleteEventObservable(): Observable<EventItem> {
    return this.deleteEventSubject.asObservable();
  }

  deleteEvent(item: EventItem): Observable<any> {
    console.log(item);
    this.deleteEventSubject.next(item);
    return this.http.post<any>(this.url + '/event/delete', {},
    { observe: 'response',
    params: {
      eventId: item.eventId.toString()
    }, headers: {
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    'Content-Type': 'application/json',
  }
});
  }


  getUpdateObservable(): Observable<boolean> {
    return this.updateEventSubject.asObservable();
  }

  updateItem(item: EventItem): Observable<any> {
    return this.http.post<any>(this.url + '/event/edit', item, {
      observe: 'response',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }
    });
  }

  collectAllReservations(id: string): Observable<any> {
    const user: User = this.userService.getLoggedUser();
    const param = new HttpParams().set('eventId', id);
    return this.http.get<any>('http://localhost:8080/event/reservations', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }, params: param
    }
    );
  }

  collectAllFreeTables(id: string): Observable<any> {
    const user: User = this.userService.getLoggedUser();
    const param = new HttpParams().set('eventId', id);
    return this.http.get<any>('http://localhost:8080/event/freeTables', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      }, params: param
    }
    );
  }


}
