import { UserService } from './../services/UserService/User.service';
import { Component, OnInit } from '@angular/core';
import { ClubService } from '../services/ClubService/Club.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  constructor(private userService: UserService, private clubService: ClubService) {

  }

  ngOnInit() {
  }

}
