import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatListModule } from '@angular/material/list';

import { LogInComponent} from './login/log-in.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { RegisterComponent } from './register/register.component';
import { HomePageComponent } from './home-page/home-page.component';
import { UserService } from './services/UserService/User.service';
import { EventComponent } from './event/event.component';
import { EventContentComponent } from './event/event-content/event-content.component';
import { AddClubComponent } from './addClub/addClub.component';
import { ClubService } from './services/ClubService/Club.service';
import { ProfileComponent } from './profile/profile.component';
import { NgTempusdominusBootstrapModule } from 'ngx-tempusdominus-bootstrap';
import { ClubTableComponent } from './club-table/club-table.component';
import { ClubTableContentComponent } from './club-table/club-table-content/club-table-content.component';
import { ReserveEventComponent } from './reserve-event/reserve-event.component';

@NgModule({
   declarations: [
      AppComponent,
      LogInComponent,
      RegisterComponent,
      NavMenuComponent,
      HomePageComponent,
      EventComponent,
      EventContentComponent,
      AddClubComponent,
      ProfileComponent,
      ClubTableComponent,
      ClubTableContentComponent,
      ReserveEventComponent,
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      AppRoutingModule,
      ReactiveFormsModule,
      HttpClientModule,
      FormsModule,
      NgTempusdominusBootstrapModule,
      MatListModule
   ],
   providers: [
      UserService,
      ClubService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
