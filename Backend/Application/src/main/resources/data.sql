INSERT INTO ROLE VALUES (1, 'USER');
INSERT INTO ROLE VALUES (2, 'CLUB_OWNER');

INSERT INTO User (user_id, NAME, SURNAME, username, password, email, date_of_birth, role) VALUES (1, 'Matija', 'Rebusa', 'matija', '$2a$10$Jo71JkzTepJjmXcfzAckU.XKkAXilR2Svq8IXlhiOQ/yZB0l5sRZm', 'matija.r@gmail.com', '1997-06-1', 1);
INSERT INTO User (user_id, NAME, SURNAME, username, password, email, date_of_birth, role, oib) VALUES (2, 'Karlo', 'Sudec', 'karlo', '$2a$10$Jo71JkzTepJjmXcfzAckU.XKkAXilR2Svq8IXlhiOQ/yZB0l5sRZm', 'karlo@gmail.com', '1997-06-10', 2, '11223344556');
INSERT INTO User (user_id, NAME, SURNAME, username, password, email, date_of_birth, role, oib) VALUES (3, 'Dario', 'Šimunović', 'dario', '$2a$10$Jo71JkzTepJjmXcfzAckU.XKkAXilR2Svq8IXlhiOQ/yZB0l5sRZm', 'simunovic@gmail.com', '1998-06-1', 2, '12345678909');
INSERT INTO User (user_id, NAME, SURNAME, username, password, email, date_of_birth, role) VALUES (4, 'Zvonimir', 'Zovak', 'zvonimir', '$2a$10$Jo71JkzTepJjmXcfzAckU.XKkAXilR2Svq8IXlhiOQ/yZB0l5sRZm', 'zzovak@gmail.com', '1999-06-1', 1);
INSERT INTO User (user_id, NAME, SURNAME, username, password, email, date_of_birth, role, oib) VALUES (5, 'Ivan', 'Ivić', 'ivan', '$2a$10$Jo71JkzTepJjmXcfzAckU.XKkAXilR2Svq8IXlhiOQ/yZB0l5sRZm', 'ivan@gmail.com', '1999-06-1', 2, '67545689763');

INSERT INTO Club (club_id, name, CONFIRMED, DESCRIPTION, LOCATION, OIB, Owner) VALUES (5, 'The Vortex',TRUE, 'Elitni klub u samom centru grada', 'Zagreb', '98765432123', 3);
INSERT INTO Club (club_id, name, CONFIRMED, DESCRIPTION, LOCATION, OIB, Owner) VALUES (6, 'The Heat', TRUE, 'Ponešto za svačiji glazbeni ukus', 'Zagreb', '98765432122', 2);
INSERT INTO Club (club_id, name, CONFIRMED, DESCRIPTION, LOCATION, OIB, Owner) VALUES (7, 'Ivanov klub', TRUE, 'Samo muzika koju Ivan voli', 'Makarska', '98765432125', 5);

INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (7, 5, 'Parter - Visoki stol', 5);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (8, 5, 'Parter - Visoki stol', 5);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (9, 8, 'Separe', 5);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (10, 10, 'VIP Separe', 5);

INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (11, 5, 'Parter - Visoki stol', 6);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (12, 7, 'Parter - Visoki stol', 6);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (13, 10, 'Separe', 6);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (14, 15, 'Mega separe', 6);
INSERT INTO Club_table (club_table_id, size, DESCRIPTION, club) VALUES (15, 8, 'Vip - vlastiti šank', 6);

INSERT INTO Event (event_id, name, date, DESCRIPTION, club) VALUES (16, 'Trash party', '2019-06-14 21:00:00', 'Ovaj petak osjetite vrućinu s nama u The Heatu!', 6);
INSERT INTO Event (event_id, name, date, DESCRIPTION, club) VALUES (17, 'Studentska srijeda', '2019-06-19 22:00:00', 'Srijedom se najviše luduje', 6);
INSERT INTO Event (event_id, name, date, DESCRIPTION, club) VALUES (18, 'CRO Trap', '2019-06-15 21:00:00', 'Subota je novi petak', 6);
INSERT INTO Event (event_id, name, date, DESCRIPTION, club) VALUES (19, 'Back in Time', '2019-07-01 21:00:00', 'Ljeto može početi', 5);
INSERT INTO Event (event_id, name, date, DESCRIPTION, club) VALUES (20, 'Jole', '2019-09-30 21:00:00', 'Jole za početak nove akademske godine.', 5);

INSERT INTO Reservation (reservation_id, reservation_owner, club_table, event) VALUES (21, 1, 13, 17);
INSERT INTO Reservation (reservation_id, reservation_owner, club_table, event) VALUES (22, 1, 10, 20);
INSERT INTO Reservation (reservation_id, reservation_owner, club_table, event) VALUES (23, 4, 8, 19);
INSERT INTO Reservation (reservation_id, reservation_owner, club_table, event) VALUES (24, 2, 14, 18);

