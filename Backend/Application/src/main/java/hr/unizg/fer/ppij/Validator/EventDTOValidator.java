package hr.unizg.fer.ppij.Validator;

import hr.unizg.fer.ppij.DTO.EventDTO;
import hr.unizg.fer.ppij.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class EventDTOValidator implements Validator {

	private static final String SIZE = "error";
	private static final String DESCRIPTION = "error";
	private static final String CLUB = "error";

	private static final int MAX_DESCRIPTION_SIZE = 120;
	private static final int MAX_SIZE = 20;

	private static final String NOT_OWNER = "not.owner";
	private static final String DESCRIPTION_WRONG_SIZE = "description.wrongSize";
	private static final String DATE_TAKEN = "date.taken";

	private final UserService userService;

	@Autowired
	public EventDTOValidator(UserService userService) {

		this.userService =userService;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return EventDTO.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		EventDTO eventDTO = (EventDTO) o;

		validateOwner(userService, errors);
		validateDescription(eventDTO, errors);
	}

	/**
	 * Checks the validity of the club
	 */
	private void validateOwner(UserService userService, Errors errors) {

		if (!userService.isOwner())
			errors.reject(CLUB, NOT_OWNER);

	}
	/**
	 * Checks the validity of the description
	 */
	private void validateDescription(EventDTO eventDTO, Errors errors) {
		String description = eventDTO.getDescription();

		if (description == null || description.length() > MAX_DESCRIPTION_SIZE) {
			errors.reject(DESCRIPTION, DESCRIPTION_WRONG_SIZE);
		}
	}

}