package hr.unizg.fer.ppij.Controllers;

import hr.unizg.fer.ppij.DTO.UserDTO;
import hr.unizg.fer.ppij.Entities.User;
import hr.unizg.fer.ppij.Service.UserService;
import hr.unizg.fer.ppij.Validator.UserDTOValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@PropertySource("classpath:userValidation.properties")
public class UserController {

	private UserService userService;
	private UserDTOValidator userDTOValidator;
	private Environment env;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public UserController(@Lazy UserService userService, UserDTOValidator userDTOValidator, Environment env) {
		this.userService = userService;
		this.userDTOValidator = userDTOValidator;
		this.env = env;
	}

	/**
	 * Registers new user
	 *
	 * @param userDTO userDTO with information needed for registration
	 * @param bindingResult binding resulr
	 * @return status ok and registered user if registration is successful or
	 *          status badRequest and list of errors if not
	 */
	@PostMapping(path = "/register", produces = "application/json")
	@Transactional
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult) {
		userDTOValidator.validate(userDTO, bindingResult);
		userDTO.setUserId(0L);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(userService.registerUser(userDTO));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError -> exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	/**
	 * Finds information about current user
	 *
	 * @return the current user
	 */
	@GetMapping(value = "/me", produces = "application/json")
	public ResponseEntity<?> getCurrentUser(){
		User user = userService.getCurrentUser();

		if (user == null) {
			return ResponseEntity.badRequest().body("Nema prijavljenog korisnika!");
		} else {
			return ResponseEntity.ok().body(modelMapper.map(user, UserDTO.class));
		}
	}

	/**
	 * update users info
	 *
	 * @param userDTO contains users new info
	 * @param bindingResult binding results
	 * @return status "ok" if successful, "badRequest" otherwise
	 */
	@PutMapping(path = "/user/edit", produces = "application/json")
	public ResponseEntity<?> editUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult) {
		userDTOValidator.validate(userDTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(userService.editInfo(userDTO));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError -> exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}


}
