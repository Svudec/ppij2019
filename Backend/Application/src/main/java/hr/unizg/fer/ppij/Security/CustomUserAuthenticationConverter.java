package hr.unizg.fer.ppij.Security;

import hr.unizg.fer.ppij.Entities.UserTokenData;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.util.StringUtils;

import java.security.Principal;
import java.util.Collection;
import java.util.Map;

@Configuration
public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {

	@Value("${token.data.user}")
	public String USER_DATA_KEY;

	public CustomUserAuthenticationConverter() {
		super();
	}

	@Override
	public Authentication extractAuthentication(Map<String, ?> map) {
		if (map.containsKey(USER_DATA_KEY)) {

			final ObjectMapper mapper = new ObjectMapper();
			final UserTokenData userTokenData = mapper.convertValue(map.get(USER_DATA_KEY), UserTokenData.class);

			Principal principal = userTokenData::getUsername;

			Collection<? extends GrantedAuthority> authorities = getAuthorities(map);
			return new UsernamePasswordAuthenticationToken(principal, "N/A", authorities);
		}

		return super.extractAuthentication(map);
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
		Object authorities = map.get(AUTHORITIES);

		if (authorities instanceof String) {
			return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);
		}

		if (authorities instanceof Collection) {
			return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils.collectionToCommaDelimitedString((Collection<?>) authorities));
		}

		throw new IllegalArgumentException("Authorities must be either a String or a Collection");
	}


}
