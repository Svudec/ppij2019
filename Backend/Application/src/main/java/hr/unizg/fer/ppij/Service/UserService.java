package hr.unizg.fer.ppij.Service;

import hr.unizg.fer.ppij.DTO.UserDTO;
import hr.unizg.fer.ppij.Entities.Club;
import hr.unizg.fer.ppij.Entities.Role;
import hr.unizg.fer.ppij.Entities.User;
import hr.unizg.fer.ppij.Repository.RoleRepository;
import hr.unizg.fer.ppij.Repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService {

	@Value("${role.user}")
	private String USER;
	@Value("${role.club_owner}")
	private String CLUB_OWNER;

	private UserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private RoleRepository roleRepository;

	private static final ModelMapper modelMapper = new ModelMapper();

	public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, RoleRepository roleRepository) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.roleRepository = roleRepository;
	}

	public User registerUser(UserDTO userDTO) {
		User user = modelMapper.map(userDTO, User.class);

		Role role  = roleRepository.findByName(USER);
		String hashPassword = bCryptPasswordEncoder.encode(user.getPassword());
		user.setRole(role);
		user.setPassword(hashPassword);
		user.setOib("-1");

		return userRepository.save(user);
	}

	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public User getCurrentUser() {
		return userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
	}

	public User editInfo(UserDTO userEditDTO) {

		User user = getCurrentUser();

		//if (userEditDTO.getName() != null) user.setName(userEditDTO.getName());

		//if (userEditDTO.getSurname() != null) user.setSurname(userEditDTO.getSurname());

		if (userEditDTO.getUsername() != null) {
			Collection<SimpleGrantedAuthority> nowAuthorities =(Collection<SimpleGrantedAuthority>)SecurityContextHolder
					.getContext().getAuthentication().getAuthorities();
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userEditDTO.getUsername(), user.getPassword(), nowAuthorities);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			user.setUsername(userEditDTO.getUsername());
		}

		if (userEditDTO.getPassword() != null) user.setPassword(bCryptPasswordEncoder.encode(userEditDTO.getPassword()));

		userRepository.save(user);

		return user;
	}

	public User markAsClubOwner(){

		User user = getCurrentUser();
		Role role = roleRepository.findByName(CLUB_OWNER);
		user.setRole(role);

		return userRepository.save(user);
	}

	public boolean isOwner(){

		Role role = getCurrentUser().getRole();

		return role.getName().equals(CLUB_OWNER);
	}

	public User markAsUser(){

		User user = getCurrentUser();
		Role role = roleRepository.findByName(USER);
		user.setRole(role);

		return userRepository.save(user);
	}

	public User assignClubToUser(Club club){

		User user = getCurrentUser();
		user.setClubOwned(club);

		return userRepository.save(user);
	}

	public User editPassword(String password) {
		String hashPassword = bCryptPasswordEncoder.encode(password);

		User user = getCurrentUser();
		user.setPassword(hashPassword);

		userRepository.save(user);
		return user;
	}

	public User editOib(String oib){

		User user = getCurrentUser();
		user.setOib(oib);

		return userRepository.save(user);
	}
}
