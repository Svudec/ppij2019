package hr.unizg.fer.ppij.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import hr.unizg.fer.ppij.Entities.Role;

import java.util.Date;

public class UserDTO {

	private Long userId;
	private String username;
	private String password;
	private String name;
	private String surname;
	private String email;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dateOfBirth;
	private Role role;

	public UserDTO() {}

	public UserDTO(String username, String password, String name, String surname, String email, Date dateOfBirth, Long userId, Role role) {
		this.username = username;
		this.name = name;
		this.password = password;
		this.surname = surname;
		this.userId = userId;
		this.email = email;
		this.dateOfBirth = dateOfBirth;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
