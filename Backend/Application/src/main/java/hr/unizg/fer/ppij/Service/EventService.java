package hr.unizg.fer.ppij.Service;

import hr.unizg.fer.ppij.DTO.EventDTO;
import hr.unizg.fer.ppij.Entities.Event;
import hr.unizg.fer.ppij.Entities.Reservation;
import hr.unizg.fer.ppij.Repository.EventRepository;
import hr.unizg.fer.ppij.Repository.ReservationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class EventService {

	private EventRepository eventRepository;
	private UserService userService;
	private ReservationRepository reservationRepository;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public EventService(ReservationRepository reservationRepository, EventRepository eventRepository, UserService userService) {
		this.eventRepository = eventRepository;
		this.userService = userService;
		this.reservationRepository = reservationRepository;
	}

	public Event createEvent(EventDTO eventDTO){

		Event event = modelMapper.map(eventDTO, Event.class);
		List<Reservation> reservations = new LinkedList<>();

		event.setClub(userService.getCurrentUser().getClubOwned());
		event.setReservations(reservations);

		return eventRepository.save(event);
	}

	public Event editEvent(EventDTO eventDTO){

		Event event = eventRepository.findByEventId(eventDTO.getEventId());

		if(event != null){
			if(eventDTO.getName() != null) event.setName(eventDTO.getName());
			if(eventDTO.getDescription() != null) event.setDescription(eventDTO.getDescription());
			if(eventDTO.getDate() != null) event.setDate(eventDTO.getDate());
		}

		eventRepository.save(event);
		return event;
	}

	public Event getEvent(Long id){

		return eventRepository.findByEventId(id);
	}

	public Event deleteEvent(Long id){
		Event event = eventRepository.findByEventId(id);

		List<Reservation> reservations = reservationRepository.findAllByEvent(event);

		for(Reservation reservation : reservations)
			reservationRepository.delete(reservation);

		eventRepository.delete(event);
		return event;
	}
}