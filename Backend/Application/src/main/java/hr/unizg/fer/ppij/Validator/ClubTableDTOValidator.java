package hr.unizg.fer.ppij.Validator;

import hr.unizg.fer.ppij.DTO.ClubTableDTO;
import hr.unizg.fer.ppij.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ClubTableDTOValidator implements Validator {

	private static final String SIZE = "error";
	private static final String DESCRIPTION = "error";
	private static final String CLUB = "error";

	private static final int MAX_DESCRIPTION_SIZE = 120;
	private static final int MAX_SIZE = 20;

	private static final String SIZE_WRONG_SIZE = "size.wrongSize";
	private static final String DESCRIPTION_WRONG_SIZE = "description.wrongSize";
	private static final String CLUB_MISSING = "club.missing";

	private final UserService userService;

	@Autowired
	public ClubTableDTOValidator(UserService userService) {
		this.userService = userService;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return ClubTableDTO.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ClubTableDTO clubTableDTO = (ClubTableDTO) o;

		validateClub(userService, errors);
		validateSize(clubTableDTO, errors);
		validateDescription(clubTableDTO, errors);
	}

	/**
	 * Checks the validity of the club
	 */
	private void validateClub(UserService userService, Errors errors) {
		if (userService.getCurrentUser() == null) {
			errors.reject(CLUB, CLUB_MISSING);
		}

		if (!userService.isOwner()){
			errors.reject(CLUB, CLUB_MISSING);
		}
	}
	/**
	 * Checks the validity of the description
	 */
	private void validateDescription(ClubTableDTO clubTableDTO, Errors errors) {
		String description = clubTableDTO.getDescription();

		if (description == null || description.length() > MAX_DESCRIPTION_SIZE) {
			errors.reject(DESCRIPTION, DESCRIPTION_WRONG_SIZE);
		}
	}

	/**
	 * Checks the validity of the size
	 */
	private void validateSize(ClubTableDTO clubTableDTO, Errors errors) {
		int size = clubTableDTO.getSize();

		if (size < 1 || size > MAX_SIZE) {
			errors.reject(SIZE, SIZE_WRONG_SIZE);
		}
	}
}
