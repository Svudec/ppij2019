package hr.unizg.fer.ppij.Validator;

import hr.unizg.fer.ppij.DTO.ReservationDTO;
import hr.unizg.fer.ppij.Entities.Club;
import hr.unizg.fer.ppij.Repository.ReservationRepository;
import hr.unizg.fer.ppij.Service.ClubTableService;
import hr.unizg.fer.ppij.Service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ReservationDTOValidator implements Validator {

	private static final String ERROR = "error";

	private static final String TABLE_MISSING = "table.missing";
	private static final String EVENT_MISSING = "event.missing";
	private static final String RESERVATION_OCCUPIED = "reservation.occupied";
	private static final String RESERVATION_INVALID = "reservation.invalid";

	private boolean noTable;
	private boolean noEvent;

	private ClubTableService clubTableService;
	private EventService eventService;
	private ReservationRepository reservationRepository;

	@Autowired
	public ReservationDTOValidator(ClubTableService clubTableService, EventService eventService, ReservationRepository reservationRepository) {
		this.clubTableService = clubTableService;
		this.eventService = eventService;
		this.reservationRepository = reservationRepository;
		noTable = false;
		noEvent = false;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return ReservationDTO.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ReservationDTO reservationDTO = (ReservationDTO) o;

		validateTable(reservationDTO, errors);
		validateEvent(reservationDTO, errors);
		if(!noEvent && !noTable)
			validateReservation(reservationDTO, errors);
		noEvent = false;
		noTable = false;
	}

	private void validateTable(ReservationDTO reservationDTO, Errors errors) {

		if(clubTableService.getClubTable(reservationDTO.getTableId()) == null) {
			errors.reject(ERROR, TABLE_MISSING);
			noTable = true;
		}
	}

	private void validateEvent(ReservationDTO reservationDTO, Errors errors) {

		if(eventService.getEvent(reservationDTO.getEventId()) == null) {
			errors.reject(ERROR, EVENT_MISSING);
			noEvent = true;
		}
	}

	private void validateReservation(ReservationDTO reservationDTO, Errors errors) {

		Club clubE = eventService.getEvent(reservationDTO.getEventId()).getClub();
		Club clubT = clubTableService.getClubTable(reservationDTO.getTableId()).getClub();

		if(!clubE.equals(clubT))
			errors.reject(ERROR, RESERVATION_INVALID);

		if(reservationRepository.findByClubTableAndEvent(clubTableService.getClubTable(reservationDTO.getTableId()),
				eventService.getEvent(reservationDTO.getEventId())) != null)
			errors.reject(ERROR, RESERVATION_OCCUPIED);
	}
}
