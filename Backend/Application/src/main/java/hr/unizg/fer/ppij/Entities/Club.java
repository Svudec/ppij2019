package hr.unizg.fer.ppij.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity(name="CLUB")
public class Club {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "club_id", nullable = false, unique = true)
    private Long clubId;

    @Column @NotNull
    private String name;

    @Column(unique = true) @NotNull
	private String oib;

    @Column @NotNull
    private String location;

    @Column
    private String description;

    @Column @NotNull
	private boolean confirmed;
    
    @OneToMany(mappedBy = "club")
    private List<Event> events;

    @OneToMany(mappedBy = "club")
    private List<ClubTable> tables;

    @OneToOne @NotNull
    @JoinColumn(name = "owner")
    private User owner;

	public @NotNull Long getClubId() {
		return clubId;
	}

	public void setClubId(@NotNull Long clubId) {
		this.clubId = clubId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<ClubTable> getTables() {
		return tables;
	}

	public void setTables(List<ClubTable> tables) {
		this.tables = tables;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getOib() {
		return oib;
	}

	public void setOib(String oib) {
		this.oib = oib;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	@Override
	public String toString() {
		return "Club{" +
				"clubId=" + clubId +
				", name='" + name + '\'' +
				", location='" + location + '\'' +
				", description='" + description + '\'' +
				", owner=" + owner +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Club)) return false;
		Club club = (Club) o;
		return clubId.equals(club.clubId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(clubId);
	}
}