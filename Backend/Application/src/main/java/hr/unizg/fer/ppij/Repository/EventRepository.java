package hr.unizg.fer.ppij.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.unizg.fer.ppij.Entities.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

	public Event findByEventId(Long id);
}
