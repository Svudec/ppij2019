package hr.unizg.fer.ppij.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity(name = "CLUB_TABLE")
public class ClubTable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "club_table_id", nullable = false, unique = true)
		private Long clubTableId;

		@Column @NotNull
		private int size;

		@Column
		private String description;
		
		@ManyToOne
		@JoinColumn(name = "club")
		private Club club;

		@OneToMany(mappedBy = "clubTable")
		private List<Reservation> reservations;

	public @NotNull Long getClubTableId() {
		return clubTableId;
	}

	public void setClubTableId(@NotNull Long clubTableId) {
		this.clubTableId = clubTableId;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public String toString() {
		return "ClubTable{" +
				"clubTableId=" + clubTableId +
				", size=" + size +
				", description='" + description + '\'' +
				", club=" + club +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ClubTable)) return false;
		ClubTable clubTable = (ClubTable) o;
		return clubTableId.equals(clubTable.clubTableId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(clubTableId);
	}
}
