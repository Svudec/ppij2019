package hr.unizg.fer.ppij.Service;

import hr.unizg.fer.ppij.DTO.ClubTableDTO;
import hr.unizg.fer.ppij.Entities.Club;
import hr.unizg.fer.ppij.Entities.ClubTable;
import hr.unizg.fer.ppij.Entities.Event;
import hr.unizg.fer.ppij.Entities.Reservation;
import hr.unizg.fer.ppij.Repository.ClubTableRepository;
import hr.unizg.fer.ppij.Repository.ReservationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClubTableService {

	private ClubTableRepository clubTableRepository;
	private UserService userService;
	private ClubService clubService;
	private ReservationRepository reservationRepository;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public ClubTableService(ClubTableRepository clubTableRepository, UserService userService, ClubService clubService, ReservationRepository reservationRepository) {
		this.clubTableRepository = clubTableRepository;
		this.userService = userService;
		this.clubService = clubService;
		this.reservationRepository = reservationRepository;
	}

	public ClubTable createClubTable(ClubTableDTO clubTableDTO){

		ClubTable clubTable = modelMapper.map(clubTableDTO, ClubTable.class);

		clubTable.setClub(userService.getCurrentUser().getClubOwned());
		clubService.refreshClubTables();

		return clubTableRepository.save(clubTable);
	}

	public ClubTable editClubTable(ClubTableDTO clubTableDTO){

		ClubTable clubTable = clubTableRepository.findByClubTableId(clubTableDTO.getClubTableId());

		if(clubTable != null){
			if(clubTableDTO.getDescription() != null) clubTable.setDescription(clubTableDTO.getDescription());
			if(clubTableDTO.getSize() != clubTableDTO.getSize()) clubTable.setSize(clubTableDTO.getSize());
		}

		clubTableRepository.save(clubTable);
		return clubTable;
	}

	public ClubTable deleteClubTable(Long id){
		ClubTable clubTable = getClubTable(id);

		clubTableRepository.delete(clubTable);
		return clubTable;
	}

	public List<ClubTable> getAllTablesFromClub(Club club){

		return clubTableRepository.findAllByClub(club);
	}

	public List<ClubTable> getFreeTables(Event event){
		List<Reservation> reservations = reservationRepository.findAllByEvent(event);
		List<ClubTable> tables = clubTableRepository.findAllByClub(event.getClub());
		List<ClubTable> freeTables = new ArrayList<>();

		for(ClubTable table : tables){
			boolean tableFree = true;

			for(Reservation reservation : reservations)
				if(reservation.getClubTable().equals(table)){
					tableFree = false;
					break;
				}

			if(tableFree)
				freeTables.add(table);
		}

		return freeTables;
	}

	public ClubTable getClubTable(Long id){
		return clubTableRepository.findByClubTableId(id);
	}
}
