package hr.unizg.fer.ppij.Controllers;

import hr.unizg.fer.ppij.DTO.ClubDTO;
import hr.unizg.fer.ppij.Entities.Club;
import hr.unizg.fer.ppij.Repository.ClubRepository;
import hr.unizg.fer.ppij.Service.ClubService;
import hr.unizg.fer.ppij.Service.UserService;
import hr.unizg.fer.ppij.Validator.ClubDTOValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@PropertySource("classpath:clubValidation.properties")
public class ClubController {

	private ClubService clubService;
	private UserService userService;
	private ClubRepository clubRepository;
	private ClubDTOValidator clubDTOValidator;
	private Environment env;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public ClubController(ClubService clubService, ClubDTOValidator clubDTOValidator, Environment env, ClubRepository clubRepository, UserService userService) {
		this.clubService = clubService;
		this.clubDTOValidator = clubDTOValidator;
		this.clubRepository = clubRepository;
		this.env = env;
		this.userService = userService;
	}

	@PostMapping(path = "/club/new", produces = "application/json")
	@Transactional
	public ResponseEntity<?> createClub(@Valid @RequestBody ClubDTO clubDTO, BindingResult bindingResult) {

		clubDTO.setName(clubDTO.getName().trim());
		clubDTO.setOib(clubDTO.getOib().trim());
		clubDTO.setOwnerOib(clubDTO.getOwnerOib().trim());
		clubDTO.setClubId(0L);
		clubDTOValidator.validate(clubDTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(modelMapper.map(clubService.createClub(clubDTO), ClubDTO.class));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError ->
					exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	@GetMapping(value = "/clubs", produces = "application/json")
	public ResponseEntity<?> getClubs(){

		List<Club> clubs = clubRepository.findAll();
		List<ClubDTO> clubDTOs = new ArrayList<>();

		for(Club club : clubs){
			ClubDTO clubDTO = modelMapper.map(club, ClubDTO.class);
			clubDTOs.add(clubDTO);
		}

		if(clubDTOs.isEmpty())
			return ResponseEntity.badRequest().body("Nema klubova");
		else
			return ResponseEntity.ok().body(clubDTOs);
	}

	@GetMapping(value = "/findClubs", produces = "application/json")
	public ResponseEntity<?> getFilteredClubs(@RequestParam String location){

		List<Club> clubs = clubRepository.findAllByLocationLike(location);
		List<ClubDTO> clubDTOs = new ArrayList<>();

		for(Club club : clubs){
			ClubDTO clubDTO = modelMapper.map(club, ClubDTO.class);
			clubDTOs.add(clubDTO);
		}

		if(clubDTOs.isEmpty())
			return ResponseEntity.badRequest().body("Nema klubova");
		else
			return ResponseEntity.ok().body(clubDTOs);
	}

	@GetMapping(value = "/clubLocations", produces = "application/json")
	public ResponseEntity<?> getClubLocations(){

		List<Club> clubs = clubRepository.findAll();
		List<String> locations = new ArrayList<>();

		for(Club club : clubs)
			if(!locations.contains(club.getLocation()))
				locations.add(club.getLocation());

		Collections.sort(locations);
		return ResponseEntity.ok().body(locations);
	}

	@GetMapping(value = "/myClub", produces = "application/json")
	public ResponseEntity<?> getMyClub(){

		if(userService.isOwner())
			return ResponseEntity.ok().body(modelMapper.map(userService.getCurrentUser().getClubOwned(), ClubDTO.class));
		else
			return ResponseEntity.badRequest().body("Korisnik nije vlasnik kluba!");
	}

	
	@GetMapping(value = "/club", produces = "application/json")
	public ResponseEntity<?> getClub(@RequestParam String clubId){
		Club club = clubRepository.findClubById(clubId);
		ClubDTO clubDTO = modelMapper.map(club, ClubDTO.class);

		if(clubDTO == null)
			return ResponseEntity.badRequest().body("Klub ne postoji");
		else
			return ResponseEntity.ok().body(clubDTO);
	}
}
