package hr.unizg.fer.ppij.Repository;

import hr.unizg.fer.ppij.Entities.Club;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClubRepository extends JpaRepository<Club, Long> {

	Club findByClubId(Long id);

	@Query(value = "select * from club c where lower(c.location) like lower(?1)", nativeQuery = true)
	List<Club> findAllByLocationLike(String location);
	
	@Query(value = "select * from club c where lower(c.club_id) like lower(?1)", nativeQuery = true)
	Club findClubById(String clubId);
}
