package hr.unizg.fer.ppij.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "RESERVATION")
public class Reservation {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "reservation_id", nullable = false, unique = true)
    private Long reservationId;
    
    @ManyToOne @NotNull
	@JoinColumn(name = "ReservationOwner")
    private User ReservationOwner;

    @ManyToOne @NotNull
	@JoinColumn(name = "clubTable")
	private ClubTable clubTable;

    @ManyToOne @NotNull
    @JoinColumn(name = "event")
    private Event event;

    public Reservation() {}

    public Reservation(User reservationOwner, ClubTable clubTable, Event event, Long reservationId){
    	this.ReservationOwner = reservationOwner;
    	this.clubTable = clubTable;
    	this.event = event;
    	this.reservationId = reservationId;
	}

	public @NotNull Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(@NotNull Long reservationId) {
		this.reservationId = reservationId;
	}

	public User getReservationOwner() {
		return ReservationOwner;
	}

	public void setReservationOwner(User reservationOwner) {
		ReservationOwner = reservationOwner;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public ClubTable getClubTable() {
		return clubTable;
	}

	public void setClubTable(ClubTable clubTable) {
		this.clubTable = clubTable;
	}

	@Override
	public String toString() {
		return "Reservation{" +
				"reservationId=" + reservationId +
				", ReservationOwner=" + ReservationOwner +
				", clubTable=" + clubTable +
				", event=" + event +
				'}';
	}
}