package hr.unizg.fer.ppij.DTO;

public class ClubDTO {

	private Long clubId;
	private String name;
	private String oib;
	private String location;
	private String description;
	private String ownerOib;

	public ClubDTO() {}

	public ClubDTO(Long clubId, String name, String location, String description, String oib, String ownerOib) {
		this.clubId = clubId;
		this.name = name;
		this.location = location;
		this.description = description;
		this.oib = oib;
		this.ownerOib = ownerOib;
	}

	public Long getClubId() {
		return clubId;
	}

	public void setClubId(Long clubId) {
		this.clubId = clubId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOib() {
		return oib;
	}

	public void setOib(String oib) {
		this.oib = oib;
	}

	public String getOwnerOib() {
		return ownerOib;
	}

	public void setOwnerOib(String ownerOib) {
		this.ownerOib = ownerOib;
	}
}
