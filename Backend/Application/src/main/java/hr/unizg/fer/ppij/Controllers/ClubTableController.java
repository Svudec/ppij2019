package hr.unizg.fer.ppij.Controllers;

import hr.unizg.fer.ppij.DTO.ClubTableDTO;
import hr.unizg.fer.ppij.Entities.ClubTable;
import hr.unizg.fer.ppij.Service.ClubService;
import hr.unizg.fer.ppij.Service.ClubTableService;
import hr.unizg.fer.ppij.Service.EventService;
import hr.unizg.fer.ppij.Validator.ClubTableDTOValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@PropertySource("classpath:clubTableValidation.properties")
public class ClubTableController {

	private ClubTableService clubTableService;
	private EventService eventService;
	private ClubService clubService;
	private ClubTableDTOValidator clubTableDTOValidator;
	private Environment env;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public ClubTableController(ClubTableService clubTableService, ClubTableDTOValidator clubTableDTOValidator, Environment env, EventService eventService, ClubService clubService) {
		this.clubTableService = clubTableService;
		this.clubTableDTOValidator = clubTableDTOValidator;
		this.env = env;
		this.eventService = eventService;
		this.clubService = clubService;
	}

	@PostMapping(path = "/table/new", produces = "application/json")
	@Transactional
	public ResponseEntity<?> createTable(@Valid @RequestBody ClubTableDTO clubTableDTO, BindingResult bindingResult) {
		clubTableDTO.setDescription(clubTableDTO.getDescription().trim());
		clubTableDTO.setClubTableId(0L);
		clubTableDTOValidator.validate(clubTableDTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(modelMapper.map(clubTableService.createClubTable(clubTableDTO), ClubTableDTO.class));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError ->
					exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	@PostMapping(path = "/table/edit", produces = "application/json")
	@Transactional
	public ResponseEntity<?> editTable(@Valid @RequestBody ClubTableDTO clubTableDTO, BindingResult bindingResult){
		clubTableDTO.setDescription(clubTableDTO.getDescription().trim());
		clubTableDTOValidator.validate(clubTableDTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(modelMapper.map(clubTableService.editClubTable(clubTableDTO), ClubTableDTO.class));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError ->
					exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	@PostMapping(path = "/table/delete", produces = "application/json")
	@Transactional
	public ResponseEntity<?> deleteTable(@RequestParam String tableId){

		Long id = Long.parseLong(tableId);
		ClubTableDTO deleted = modelMapper.map(clubTableService.deleteClubTable(id), ClubTableDTO.class);

		if(deleted != null)
			return ResponseEntity.ok().body(deleted);
		else
			return ResponseEntity.badRequest().body("Ne postoji klub s tim ID");
	}

	@GetMapping(path = "/event/freeTables", produces = "application/json")
	@Transactional
	public ResponseEntity<List<ClubTableDTO>> getFreeTables(@RequestParam String eventId){

		Long id = Long.parseLong(eventId);
		List<ClubTableDTO> clubTableDTOS = new ArrayList<>();
		List<ClubTable> clubTables = clubTableService.getFreeTables(eventService.getEvent(id));

		for(ClubTable clubTable : clubTables){
			clubTableDTOS.add(modelMapper.map(clubTable, ClubTableDTO.class));
		}

		return ResponseEntity.ok().body(clubTableDTOS);
	}

	@GetMapping(path = "/club/tables", produces = "application/json")
	@Transactional
	public ResponseEntity<List<ClubTableDTO>> getClubTables(@RequestParam String clubId){

		Long id = Long.parseLong(clubId);
		List<ClubTableDTO> clubTableDTOS = new ArrayList<>();
		List<ClubTable> clubTables = clubTableService.getAllTablesFromClub(clubService.getClub(id));

		for(ClubTable clubTable : clubTables){
			clubTableDTOS.add(modelMapper.map(clubTable, ClubTableDTO.class));
		}

		return ResponseEntity.ok().body(clubTableDTOS);
	}
}
