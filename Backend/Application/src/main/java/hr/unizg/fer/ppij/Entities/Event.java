package hr.unizg.fer.ppij.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Entity(name = "EVENT")
public class Event {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "event_id", nullable = false, unique = true)
    private Long eventId;

    @Column @NotNull
    private String name;

    @Column @NotNull
	private LocalDateTime date;

    @Column
    private String description;
    
    @OneToMany(mappedBy = "event")
    private List<Reservation> reservations;

    @ManyToOne
    @JoinColumn(name = "club") @NotNull
    private Club club;

	public @NotNull Long getEventId() {
		return eventId;
	}

	public void setEventId(@NotNull Long eventId) {
		this.eventId = eventId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

}