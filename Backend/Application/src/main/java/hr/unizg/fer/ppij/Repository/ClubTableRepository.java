package hr.unizg.fer.ppij.Repository;

import hr.unizg.fer.ppij.Entities.Club;
import hr.unizg.fer.ppij.Entities.ClubTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClubTableRepository extends JpaRepository<ClubTable, Long> {

	List<ClubTable> findAllByClub(Club club);

	ClubTable findByClubTableId(Long id);
}
