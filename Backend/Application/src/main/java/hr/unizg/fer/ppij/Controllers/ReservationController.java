package hr.unizg.fer.ppij.Controllers;

import hr.unizg.fer.ppij.DTO.ClubDTO;
import hr.unizg.fer.ppij.DTO.ClubTableDTO;
import hr.unizg.fer.ppij.DTO.EventDTO;
import hr.unizg.fer.ppij.DTO.ReservationDTO;
import hr.unizg.fer.ppij.Entities.Reservation;
import hr.unizg.fer.ppij.Repository.ReservationRepository;
import hr.unizg.fer.ppij.Service.EventService;
import hr.unizg.fer.ppij.Service.ReservationService;
import hr.unizg.fer.ppij.Service.UserService;
import hr.unizg.fer.ppij.Validator.ReservationDTOValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@PropertySource("classpath:reservationValidation.properties")
public class ReservationController {

	private ReservationService reservationService;
	private ReservationRepository reservationRepository;
	private UserService userService;
	private EventService eventService;
	private Environment env;
	private ReservationDTOValidator reservationDTOValidator;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public ReservationController(ReservationDTOValidator reservationDTOValidator, ReservationService reservationService, Environment env, ReservationRepository reservationRepository, UserService userService, EventService eventService) {
		this.reservationService = reservationService;
		this.env = env;
		this.reservationRepository = reservationRepository;
		this.userService = userService;
		this.eventService = eventService;
		this.reservationDTOValidator = reservationDTOValidator;
	}

	@PostMapping(path = "/reservation/new", produces = "application/json")
	@Transactional
	public ResponseEntity<?> createReservation(@Valid @RequestBody ReservationDTO reservationDTO, BindingResult bindingResult) {
		reservationDTO.setReservationId(0L);
		reservationDTOValidator.validate(reservationDTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(modelMapper.map(reservationService.createReservation(reservationDTO), ReservationDTO.class));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError ->
					exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	@GetMapping(value = "/myReservations", produces = "application/json")
	public ResponseEntity<List<ReservationDTO>> getMyReservations(){

		List<Reservation> reservations = reservationRepository.findAllByReservationOwner(userService.getCurrentUser());
		List<ReservationDTO> reservationDTOS = new ArrayList<>();

		for(Reservation reservation : reservations){
			ReservationDTO reservationDTO = modelMapper.map(reservation, ReservationDTO.class);
			EventDTO eventDTO = modelMapper.map(reservation.getEvent(), EventDTO.class);
			eventDTO.setClubDTO(modelMapper.map(eventService.getEvent(eventDTO.getEventId()).getClub(), ClubDTO.class));

			reservationDTO.setEventDTO(eventDTO);
			reservationDTO.setClubTableDTO(modelMapper.map(reservation.getClubTable(), ClubTableDTO.class));
			reservationDTOS.add(reservationDTO);
		}
		return ResponseEntity.ok().body(reservationDTOS);

	}

	@GetMapping(value = "/event/reservations", produces = "application/json")
	public ResponseEntity<List<ReservationDTO>> getMyClubReservations(@RequestParam Long eventId){

		List<Reservation> reservations = reservationRepository.findAllByEvent(eventService.getEvent(eventId));
		List<ReservationDTO> reservationDTOS = new ArrayList<>();

		for(Reservation reservation : reservations){
			ReservationDTO reservationDTO = modelMapper.map(reservation, ReservationDTO.class);
			reservationDTO.setEventDTO(modelMapper.map(reservation.getEvent(), EventDTO.class));
			reservationDTO.setClubTableDTO(modelMapper.map(reservation.getClubTable(), ClubTableDTO.class));
			reservationDTOS.add(reservationDTO);
		}

		return ResponseEntity.ok().body(reservationDTOS);

	}
}