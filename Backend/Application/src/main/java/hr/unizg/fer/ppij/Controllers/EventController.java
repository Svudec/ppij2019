package hr.unizg.fer.ppij.Controllers;

import hr.unizg.fer.ppij.DTO.ClubDTO;
import hr.unizg.fer.ppij.DTO.EventDTO;
import hr.unizg.fer.ppij.Entities.Event;
import hr.unizg.fer.ppij.Repository.EventRepository;
import hr.unizg.fer.ppij.Service.EventService;
import hr.unizg.fer.ppij.Service.UserService;
import hr.unizg.fer.ppij.Validator.EventDTOValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@PropertySource("classpath:eventValidation.properties")
public class EventController {

	private EventService eventService;
	private EventRepository eventRepository;
	private UserService userService;
	private Environment env;
	private EventDTOValidator eventDTOValidator;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public EventController(EventDTOValidator eventDTOValidator, EventService eventService, Environment env, EventRepository eventRepository, UserService userService) {
		this.eventService = eventService;
		this.eventRepository = eventRepository;
		this.env = env;
		this.userService = userService;
		this.eventDTOValidator = eventDTOValidator;
	}

	@PostMapping(path = "/event/new", produces = "application/json")
	@Transactional
	public ResponseEntity<?> createEvent(@Valid @RequestBody EventDTO eventDTO, BindingResult bindingResult) {
		eventDTO.setDescription(eventDTO.getDescription().trim());
		eventDTO.setName(eventDTO.getName().trim());
		eventDTO.setEventId(0L);
		eventDTOValidator.validate(eventDTO, bindingResult);

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(modelMapper.map(eventService.createEvent(eventDTO), EventDTO.class));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError ->
					exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	@PostMapping(path = "/event/edit", produces = "application/json")
	@Transactional
	public ResponseEntity<?> editEvent(@Valid @RequestBody EventDTO eventDTO, BindingResult bindingResult){
		eventDTO.setDescription(eventDTO.getDescription().trim());
		eventDTO.setName(eventDTO.getName().trim());

		if (!bindingResult.hasErrors()) {
			return ResponseEntity.ok(modelMapper.map(eventService.editEvent(eventDTO), EventDTO.class));
		} else {
			List<ObjectError> errorList = bindingResult.getAllErrors();
			Map<String, String> exceptions = new HashMap<>();

			bindingResult.getAllErrors().forEach(objectError ->
					exceptions.put(objectError.getCode(), env.getProperty(objectError.getDefaultMessage())));

			return ResponseEntity.badRequest().body(exceptions);
		}
	}

	@PostMapping(path = "/event/delete", produces = "application/json")
	@Transactional
	public ResponseEntity<?> deleteEvent(@RequestParam String eventId){

		Long id = Long.parseLong(eventId);
		EventDTO deleted = modelMapper.map(eventService.deleteEvent(id), EventDTO.class);

		if(deleted != null)
			return ResponseEntity.ok().body(deleted);
		else
			return ResponseEntity.badRequest().body("Ne postoji event s tim ID");
	}

	@GetMapping(value = "/myEvents", produces = "application/json")
	public ResponseEntity<List<EventDTO>> getMyEvents(){

		List<Event> events = eventRepository.findAll();
		List<EventDTO> eventDTOS = new ArrayList<>();

		for(Event event : events){
			if(event.getClub().equals(userService.getCurrentUser().getClubOwned())) {

				EventDTO eventDTO = modelMapper.map(event, EventDTO.class);
				eventDTO.setClubDTO(modelMapper.map(event.getClub(), ClubDTO.class));
				eventDTOS.add(eventDTO);
			}
		}
			return ResponseEntity.ok().body(eventDTOS);
	}

	@GetMapping(value = "/events", produces = "application/json")
	public ResponseEntity<List<EventDTO>> getEvents(){

		List<Event> events = eventRepository.findAll();
		List<EventDTO> eventDTOS = new ArrayList<>();

		for(Event event : events){

			EventDTO eventDTO = modelMapper.map(event, EventDTO.class);
			eventDTO.setClubDTO(modelMapper.map(event.getClub(), ClubDTO.class));
			eventDTOS.add(eventDTO);
		}
		return ResponseEntity.ok().body(eventDTOS);
	}
	
	@GetMapping(value = "/currentEvents", produces = "application/json")
	public ResponseEntity<List<EventDTO>> getCurrentEvents(@RequestParam String clubId){

		List<Event> events = eventRepository.findAll();
		List<EventDTO> eventDTOS = new ArrayList<>();

		for(Event event : events){
			if(event.getClub().getClubId().toString().equals(clubId)) {

				EventDTO eventDTO = modelMapper.map(event, EventDTO.class);
				eventDTO.setClubDTO(modelMapper.map(event.getClub(), ClubDTO.class));
				eventDTOS.add(eventDTO);
			}
		}
			return ResponseEntity.ok().body(eventDTOS);
	}
}