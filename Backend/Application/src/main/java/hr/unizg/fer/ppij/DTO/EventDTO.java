package hr.unizg.fer.ppij.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class EventDTO {

	private Long eventId;
	private String name;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss")
	private LocalDateTime date;
	private String description;
	private ClubDTO clubDTO;

	public EventDTO() {}

	public EventDTO(Long eventId, String name, LocalDateTime date, String description) {
		this.eventId = eventId;
		this.name = name;
		this.date = date;
		this.description = description;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ClubDTO getClubDTO() {
		return clubDTO;
	}

	public void setClubDTO(ClubDTO clubDTO) {
		this.clubDTO = clubDTO;
	}
}
