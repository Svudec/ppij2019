package hr.unizg.fer.ppij.Validator;

import hr.unizg.fer.ppij.DTO.UserDTO;
import hr.unizg.fer.ppij.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Component
public class UserDTOValidator implements Validator {

	private static final String NAME = "error";
	private static final String SURNAME = "error";
	private static final String USERNAME = "error";
	private static final String PASSWORD = "error";
	private static final String EMAIL = "error";
	private static final String DATE_OF_BIRTH = "error";

	private static final int MIN_PASSWORD_SIZE = 5;
	private static final int AGE_TOO_YOUNG = 18;
	private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
			Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	private final UserRepository userRepository;

	private static final String NAME_IS_EMPTY = "name.empty";
	private static final String NAME_ILLEGAL_CHARS = "name.illegalChars";

	private static final String EMAIL_IS_EMPTY = "email.empty";
	private static final String EMAIL_ALREADY_EXISTS = "email.alreadyExists";
	private static final String EMAIL_BAD_FORMAT = "email.badFormat";

	private static final String SURNAME_IS_EMPTY = "lastname.empty";
	private static final String SURNAME_ILLEGAL_CHARS = "lastname.illegalChars";

	private static final String USERNAME_IS_EMPTY = "username.empty";
	private static final String USERNAME_ALREADY_EXISTS = "username.alreadyExists";

	private static final String PASSWORD_IS_EMPTY = "password.empty";
	private static final String PASSWORD_WRONG_SIZE = "password.wrongSize";

	private static final String DATE_OF_BIRTH_IS_EMPTY = "dateOfBirth.empty";
	private static final String DATE_OF_BIRTH_BAD_AGE = "dateOfBirth.badAge";
	private static final String DATE_OF_BIRTH_ILLEGAL_DATE = "dateOfBirth.illegalAge";



	@Autowired
	public UserDTOValidator(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return UserDTO.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		UserDTO userDTO = (UserDTO) o;

		validateName(userDTO, errors);
		validateSurname(userDTO, errors);
		validateUsername(userDTO, errors);
		validatePassword(userDTO, errors);
		validateEmail(userDTO, errors);
		validateDateOfBirth(userDTO, errors);
	}

	/**
	 * Checks the validity of the name
	 */
	private void validateName(UserDTO userDTO, Errors errors) {
		String name = userDTO.getName();

		if (name == null || name.isEmpty()){
			errors.reject(NAME, NAME_IS_EMPTY);
		} else if (!name.trim().chars().allMatch(Character::isLetter)) {
			errors.reject(NAME, NAME_ILLEGAL_CHARS);
		}
	}

	/**
	 * Checks the validity of the surname
	 */
	private void validateSurname(UserDTO userDTO, Errors errors) {
		String surname = userDTO.getSurname();

		if (surname == null || surname.isEmpty()){
			errors.reject(SURNAME, SURNAME_IS_EMPTY);
		} else if (!surname.trim().chars().allMatch(Character::isLetter)) {
			errors.reject(SURNAME, SURNAME_ILLEGAL_CHARS);
		}
	}
	/**
	 * Checks the validity of the username
	 */
	private void validateUsername(UserDTO userDTO, Errors errors) {
		String username = userDTO.getUsername();

		if (username == null || username.isEmpty()) {
			errors.reject(USERNAME, USERNAME_IS_EMPTY);
		} else if (userRepository.findByUsername(username) != null) {
			errors.reject(USERNAME, USERNAME_ALREADY_EXISTS);
		}
	}

	/**
	 * Checks the validity of the password
	 */
	private void validatePassword(UserDTO userDTO, Errors errors) {
		String password = userDTO.getPassword();

		if (password == null) {
			errors.reject(PASSWORD, PASSWORD_IS_EMPTY);
		} else if (password.length() < MIN_PASSWORD_SIZE ) {
			errors.reject(PASSWORD, PASSWORD_WRONG_SIZE);
		}
	}

	private void validateEmail(UserDTO userDTO, Errors errors) {
		String email = userDTO.getEmail();

		if (email == null || email.isEmpty()) {
			errors.reject(EMAIL, EMAIL_IS_EMPTY);
		}else if (!VALID_EMAIL_ADDRESS_REGEX.matcher(email).matches()) {
			errors.reject(EMAIL, EMAIL_BAD_FORMAT);
		}else if (userRepository.findByEmail(email) != null) {
			errors.reject(EMAIL, EMAIL_ALREADY_EXISTS);
		}
	}

	private void validateDateOfBirth(UserDTO userDTO, Errors errors) {
		if (userDTO.getDateOfBirth() == null) {
			errors.reject(DATE_OF_BIRTH, DATE_OF_BIRTH_IS_EMPTY);
			return;
		}

		Date dateOfBirth = userDTO.getDateOfBirth();
		Date today = new Date();
		if (dateOfBirth.after(today)) {
			errors.reject(DATE_OF_BIRTH, DATE_OF_BIRTH_ILLEGAL_DATE);
			return;
		}

		long yearDifference = TimeUnit.MILLISECONDS.toDays(today.getTime() - dateOfBirth.getTime()) / 365;

		if (yearDifference < AGE_TOO_YOUNG) {
			errors.reject(DATE_OF_BIRTH, DATE_OF_BIRTH_BAD_AGE);
		}
	}
}
