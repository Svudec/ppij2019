package hr.unizg.fer.ppij.DTO;

public class ReservationDTO {

	private Long reservationId;
	private Long tableId;
	private Long eventId;
	private EventDTO eventDTO;
	private ClubTableDTO clubTableDTO;

	public ReservationDTO() {}

	public ReservationDTO(Long tableId, Long eventId) {
		this.tableId = tableId;
		this.eventId = eventId;
	}

	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public EventDTO getEventDTO() {
		return eventDTO;
	}

	public void setEventDTO(EventDTO eventDTO) {
		this.eventDTO = eventDTO;
	}

	public ClubTableDTO getClubTableDTO() {
		return clubTableDTO;
	}

	public void setClubTableDTO(ClubTableDTO clubTableDTO) {
		this.clubTableDTO = clubTableDTO;
	}
}
