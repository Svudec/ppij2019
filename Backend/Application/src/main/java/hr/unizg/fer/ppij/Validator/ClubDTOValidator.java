package hr.unizg.fer.ppij.Validator;

import hr.unizg.fer.ppij.DTO.ClubDTO;
import hr.unizg.fer.ppij.Entities.Role;
import hr.unizg.fer.ppij.Repository.ClubRepository;
import hr.unizg.fer.ppij.Repository.RoleRepository;
import hr.unizg.fer.ppij.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ClubDTOValidator implements Validator {

	private static final String NAME = "error";
	private static final String ADMIN = "error";
	private static final String OIB = "error";

	private static final int MAX_NAME_SIZE = 64;
	private static final int MIN_NAME_SIZE = 1;
	private static final int OIB_LENGTH = 11;

	private static final String NAME_WRONG_SIZE = "name.wrongSize";
	private static final String ADMIN_NOT_FOUND = "admin.notFound";
	private static final String ADMIN_OWNER = "name.alreadyExists";
	private static final String OWNER_OIB_WRONG = "ownerOib.wrongSize";
	private static final String OIB_WRONG = "OIB.wrongSize";

	private final UserService userService;

	@Autowired
	public ClubDTOValidator(UserService userService) {
		this.userService = userService;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return ClubDTO.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ClubDTO clubDTO = (ClubDTO) o;

		validateAdmin(userService, errors);
		validateName(clubDTO, errors);
		validateOibs(clubDTO, errors);
	}

	/**
	 * Checks the validity of the admin
	 */
	private void validateAdmin(UserService userService, Errors errors) {
		if (userService.getCurrentUser() == null) {
			errors.reject(ADMIN, ADMIN_NOT_FOUND);
		}

		if (userService.isOwner()){
			errors.reject(ADMIN,ADMIN_OWNER);
		}
	}
	/**
	 * Checks the validity of the name
	 */
	private void validateName(ClubDTO clubDTO, Errors errors) {
		String name = clubDTO.getName();

		if (name == null || name.length() < MIN_NAME_SIZE || name.length() > MAX_NAME_SIZE) {
			errors.reject(NAME, NAME_WRONG_SIZE);
		}
	}

	private void validateOibs(ClubDTO clubDTO, Errors errors){

		String oib = clubDTO.getOib();
		String ownerOib = clubDTO.getOwnerOib();

		if(oib.length() != OIB_LENGTH || !oib.matches("[0-9]*")){
			errors.reject(OIB, OIB_WRONG);
		}

		if(ownerOib.length() != OIB_LENGTH || !ownerOib.matches("[0-9]*")){
			errors.reject(OIB, OWNER_OIB_WRONG);
		}
	}
}
