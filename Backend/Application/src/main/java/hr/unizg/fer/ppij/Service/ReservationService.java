package hr.unizg.fer.ppij.Service;

import hr.unizg.fer.ppij.DTO.ReservationDTO;
import hr.unizg.fer.ppij.Entities.ClubTable;
import hr.unizg.fer.ppij.Entities.Event;
import hr.unizg.fer.ppij.Entities.Reservation;
import hr.unizg.fer.ppij.Entities.User;
import hr.unizg.fer.ppij.Repository.ReservationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {

	private ReservationRepository reservationRepository;
	private UserService userService;
	private EventService eventService;
	private ClubTableService clubTableService;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public ReservationService(ReservationRepository reservationRepository, UserService userService, EventService eventService, ClubTableService clubTableService) {
		this.reservationRepository = reservationRepository;
		this.userService = userService;
		this.eventService = eventService;
		this.clubTableService = clubTableService;
	}

	public Reservation createReservation(ReservationDTO reservationDTO){

		User user = userService.getCurrentUser();
		Event event = eventService.getEvent(reservationDTO.getEventId());
		ClubTable table = clubTableService.getClubTable(reservationDTO.getTableId());

		Reservation reservation = new Reservation(user, table, event, reservationDTO.getReservationId());

		return reservationRepository.save(reservation);
	}
}