package hr.unizg.fer.ppij.Repository;

import hr.unizg.fer.ppij.Entities.ClubTable;
import hr.unizg.fer.ppij.Entities.Event;
import hr.unizg.fer.ppij.Entities.Reservation;
import hr.unizg.fer.ppij.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

	public List<Reservation> findAllByEvent(Event event);

	@Query(value = "select * from reservation r where r.reservation_owner = ?1", nativeQuery = true)
	public List<Reservation> findAllByReservationOwner(User owner);

	public Reservation findByClubTableAndEvent(ClubTable table, Event event);

}
