package hr.unizg.fer.ppij.Security;

import hr.unizg.fer.ppij.Entities.UserTokenData;
import hr.unizg.fer.ppij.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Component
public class CustomTokenConverter extends JwtAccessTokenConverter {

	@Value("${token.data.user}")
	String USER_DATA_KEY;

	@Autowired
	private UserService userService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication){
		final Map<String, Object> additionalInfo = new HashMap<>();
		UserTokenData userTokenData = new UserTokenData();

		userTokenData.setId(userService.getUserByUsername(authentication.getName()).getUserId());
		userTokenData.setUsername(authentication.getName());
		userTokenData.setRole(userService.getUserByUsername(authentication.getName()).getRole().getName());

		additionalInfo.put(USER_DATA_KEY, userTokenData);

		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
	}

	@Override
	public OAuth2Authentication extractAuthentication(Map<String, ?> claims) {
		OAuth2Authentication authentication = super.extractAuthentication(claims);
		authentication.setDetails(claims);
		return authentication;
	}

}
