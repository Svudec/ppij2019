package hr.unizg.fer.ppij.DTO;

public class ClubTableDTO {

	private Long clubTableId;
	private int size;
	private String description;

	public ClubTableDTO() {}

	public ClubTableDTO(Long clubTableId, int size, String description) {
		this.clubTableId = clubTableId;
		this.size = size;
		this.description = description;
	}

	public Long getClubTableId() {
		return clubTableId;
	}

	public void setClubTableId(Long clubTableId) {
		this.clubTableId = clubTableId;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
