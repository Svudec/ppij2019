package hr.unizg.fer.ppij.Entities;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity(name = "USER")
public class User {

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "user_id", nullable = false, unique = true)
    private Long userId;

    @Column(unique = true) @NotNull
	private String username;

    @Column @NotNull
	private String password;

    @Column(unique = true) @NotNull
	private String email;

    @Column @NotNull
    private String name;

    @Column @NotNull
    private String surname;

    @Column @NotNull
	private Date dateOfBirth;

    @ColumnDefault("-1") @NotNull
	private String oib;
    
    @OneToMany(mappedBy = "ReservationOwner")
    private List<Reservation> reservations;

    @OneToOne(mappedBy = "owner")
    private Club clubOwned;

	@ManyToOne
	@JoinColumn (name="role")
	private Role role;

	public @NotNull Long getUserId() {
		return userId;
	}

	public void setUserId(@NotNull Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOib() {
		return oib;
	}

	public void setOib(String oib) {
		this.oib = oib;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Club getClubOwned() {
		return clubOwned;
	}

	public void setClubOwned(Club clubOwned) {
		this.clubOwned = clubOwned;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User{" +
				"userId=" + userId +
				", username='" + username + '\'' +
				", name='" + name + '\'' +
				", surname='" + surname + '\'' +
				'}';
	}
}