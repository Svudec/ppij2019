package hr.unizg.fer.ppij.Repository;

import hr.unizg.fer.ppij.Entities.Role;
import hr.unizg.fer.ppij.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByName(String name);
}
