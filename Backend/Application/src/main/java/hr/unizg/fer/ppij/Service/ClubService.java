package hr.unizg.fer.ppij.Service;

import hr.unizg.fer.ppij.DTO.ClubDTO;
import hr.unizg.fer.ppij.Entities.Club;
import hr.unizg.fer.ppij.Entities.ClubTable;
import hr.unizg.fer.ppij.Entities.Event;
import hr.unizg.fer.ppij.Repository.ClubRepository;
import hr.unizg.fer.ppij.Repository.ClubTableRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClubService {

	private ClubRepository clubRepository;
	private UserService userService;
	private ClubTableRepository clubTableRepository;

	private static final ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public ClubService(ClubRepository clubRepository, UserService userService, ClubTableRepository clubTableRepository){
		this.clubRepository = clubRepository;
		this.userService = userService;
		this.clubTableRepository = clubTableRepository;
	}

	public Club createClub(ClubDTO clubDTO){

		Club club = modelMapper.map(clubDTO, Club.class);
		List<Event> events = new ArrayList<>();
		List<ClubTable> tables = new ArrayList<>();

		club.setOwner(userService.getCurrentUser());
		club.setEvents(events);
		club.setTables(tables);
		club.setConfirmed(false);

		userService.editOib(clubDTO.getOwnerOib());
		userService.markAsClubOwner();
		userService.assignClubToUser(club);

		return clubRepository.save(club);
	}

	public Club editClub(ClubDTO clubDTO){
		Club club = clubRepository.findByClubId(clubDTO.getClubId());

		if (clubDTO.getName() != null) club.setName(clubDTO.getName());
		if (clubDTO.getDescription() != null) club.setDescription(clubDTO.getDescription());
		if (clubDTO.getLocation() != null) club.setLocation(clubDTO.getLocation());

		return clubRepository.save(club);
	}

	public void refreshClubTables(){

		Club club = userService.getCurrentUser().getClubOwned();
		List<ClubTable> newTables = clubTableRepository.findAllByClub(club);

		System.out.println(club);
		if (newTables != null){
			club.setTables(newTables);}

		clubRepository.save(club);
	}

	public Club getClub(Long id){
		return clubRepository.findByClubId(id);
	}
}
